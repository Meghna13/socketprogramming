import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.ArrayList;
import java.awt.List;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

public class SocClient {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		String ip = "localhost";
		int port = 9988;
		Socket s = new Socket(ip,port);
		byte[]b = new byte[2002];
		FileOutputStream fos = new FileOutputStream("/Users/Meghna/Downloads/Coding skills test/islands_in_the_stream_client.txt");
		InputStream in = s.getInputStream();
		in.read(b, 0, b.length);
		fos.write(b,0,b.length);
		countwords();
		//System.out.println(textWords);
		
	}
	
	//get the count of each word
	private static void countwords() throws Exception {
		// TODO Auto-generated method stub
		String line;
		Path path = Paths.get(System.getProperty("user.dir")).resolve("/Users/Meghna/Downloads/Coding skills test/islands_in_the_stream_client.txt");
		BufferedReader bufferReader = new BufferedReader(new FileReader(path.toFile()));
		Map<String,Integer> wordcountFrequency = new HashMap<String,Integer>();
		line = bufferReader.readLine();
		while(line != null) {
			//System.out.println(line);
			if(!line.trim().equals("")){
				//System.out.println(line);
				String [] words = line.split(" ");
				
				for(String word: words){
					if(word == null || word.trim().equals("")){
						continue;
					}
					String processed = word.toLowerCase();
					if(wordcountFrequency.containsKey(processed)){
						wordcountFrequency.put(processed, wordcountFrequency.get(processed)+1);
						
					}else{
						wordcountFrequency.put(processed, 1);
					}
				}
			}
			line = bufferReader.readLine();
			
		}
		//System.out.println(wordcountFrequency);
		//System.out.println(wordcountFrequency.size());
		if(wordcountFrequency.size() > 10) {
			Map<String, Integer> sortedByCount = sortByValue(wordcountFrequency);
	        System.out.println("The first 10 most occured words from the text file are: " +  "\n" + sortedByCount);

		}
		
	}

	private static Map<String, Integer> sortByValue(Map<String, Integer> wordcountFrequency) {
		// TODO Auto-generated method stub
		int n = 10;
		return wordcountFrequency.entrySet()
                .stream()
                .sorted((Map.Entry.<String, Integer>comparingByValue().reversed()))
                .limit(n)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
	}

}    


